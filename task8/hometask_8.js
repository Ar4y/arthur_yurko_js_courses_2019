function request(url) {
    return new Promise((res, rej) => {
        const delayTime = Math.floor(Math.random() * 10000) + 1;

        setTimeout(() => res(url), delayTime);
    });
}

function promiseAll(urlsArr) {
	return new Promise((resolve, reject) => {
		let responseArr = [];
		urlsArr.reduce((promise, url, index) => {
			return promise
				.then(() => request(url))
				.then(responseArr.push.bind(responseArr))
				.then(() => {
					if (index === urlsArr.length - 1) {
						resolve(responseArr)
					}
				});
		}, Promise.resolve());
	});
}