function debounce(callback, delay) {
	let timerId;
	return function() {
		clearTimeout(timerId);
		timerId = setTimeout(callback, delay, ...arguments);
	}
}

function log(num) {
  console.log(num);
}

const debouncedFirst = debounce(log, 500);
const debouncedSecond = debounce(log, 500);
const debouncedThird = debounce(log, 500);

debouncedFirst('1');
debouncedFirst('2');
// --- 500 ms ---
// '2'
debouncedSecond('3');
// --- 500 ms ---
// '3'
debouncedThird('4');
debouncedThird('5');
debouncedThird('6');
// --- 500 ms ---
// '6'
