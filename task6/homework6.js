// Task 1.
// Создайте функцию, которая на вход получаете строку "a.b.c.d",
// а на выходе возвращает объект: { a: { b: { c: { d: null } } } }

function createNestedObjects(inputStr) {
	function callback(acc, curentKey) {
		const keyInObj = Object.keys(acc)[0];
		if (keyInObj === undefined) {
			acc[curentKey] = null;
		} else if (acc[keyInObj] === null) {
			acc[keyInObj] = {[curentKey]: null};
		} else {
			callback(acc[keyInObj], curentKey);
		}

		return acc;
	} 

	return inputStr.split(".").reduce(callback, {});
}

// Task 2
// Написать функцию, которая находит пересечение двух массивов
// Например - результатом вызовом findEquals([2, 6, 3, 7, 5], [1, 2, 3, 7]) будет  масив [2, 3, 7]

function findEquals(firstArr, secondArr) {
	return firstArr.filter((elem) => {
		return secondArr.includes(elem);
	})
}

// Task 3
// Напишите функцию для объединения двух массивов и удаления всех дублирующих элементов.
const firstArray = [1, 2, 3]; 
const secondArray = [2, 30, 1]; 
const mergedArray = mergeArray(firstArray, secondArray); // [1, 2, 30, 3]

function mergeArray(firstArr, secondArr) {
	return secondArr.reduce((acc, currentElem) => {
		if (!acc.includes(currentElem)) {
			acc.push(currentElem);
		}

		return acc;
	}, firstArr)
}

// Task 4
// Напишите функцию mapObject, которая преобразовывает значения
// всех свойств объекта (аналог map из массива, только для объекта, возвращает новый объект)
const double = x => x * 2;
mapObject(double, {x: 1, y: 2, z: 3}); //=> {x: 2, y: 4, z: 6}

function mapObject(callback, inputObj) {
	let obj = {...inputObj};
	for (let key in obj) {
		obj[key] = callback(obj[key]);
	}

	return obj;
}