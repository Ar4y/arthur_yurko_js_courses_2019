// Task 1
// Ваша задача - заполнить пробелы (_______________________).
// Функция multiplier принимает x один параметр и возвращает анонимную функцию, которая принимает один параметр - y,
// и возвращает произведение x * y.

function multiplier(x) {
    return function(y) {
        return x * y;
    }
}

function processData(input) {
    const waterWeight = multiplier(1000);
    const mercuryWeight = multiplier(1355);
    const oilWeight = multiplier(900);
    
    console.log("Weight of " + input + " metric cube of water = " + waterWeight(input) + " kg");
    console.log("Weight of " + input + " metric cube of mercury = " + mercuryWeight(input) + " kg");
    console.log("Weight of " + input + " metric cube of oil = " + oilWeight(input) + " kg");
}

// Task 2
// Написать функцию makeRandomFn которя принимает диапазон чисел и возвращяет функцию,
// которая при вызове возвращяет случайное число c этого диапазона.

function makeRandomFn(input) {
    let arrNum;
    if (typeof input === "object") {
        arrNum = input;
    } else {
        arrNum = arguments;
    }
    return () => {
        let randomIndex = Math.floor(Math.random() * arrNum.length);
        return arrNum[randomIndex]; 
    }
}

const getRandomNumber = makeRandomFn([1, 2, 100, 34, 45, 556, 33])
console.log(getRandomNumber()) // 556
console.log(getRandomNumber()) // 100
console.log(getRandomNumber()) // 2

// Task 3
// Нужно расширить функцию makeRandomFn, таким образом
// чтобы можно было передавать диапазон не только через масив, а как аргументы через запятую

makeRandomFn(1, 2, 100, 34, 45, 556, 33)
makeRandomFn([1, 2, 100, 34, 45, 556, 33])
