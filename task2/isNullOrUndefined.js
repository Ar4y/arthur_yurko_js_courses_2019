function isNullOrUndefined(input) {
	return input === null || input === undefined;
}