class MarkupGenerator {
	constructor(calculator) {
		this.generateMarkup(calculator);
	}
	generateMarkup(calculator) {
		calculator.root = this.createElem("div", {id: "root"});
		calculator.display = this.createElem("div", {id: "display"});
		calculator.numpad = this.generateNumpadMarkup();
		calculator.history = this.createElem("div", {id: "history"});
		calculator.historyButtn = this.createElem("img", {
			id: "history-button", 
			src: "img/history.png"
		});

		const div = this.createElem("div");
		div.appendChild(this.createEquationDiv());
		calculator.display.appendChild(div);
		calculator.history.appendChild(this.createElem("div"));
		calculator.root.appendChild(calculator.display);
		calculator.root.appendChild(calculator.numpad);
		calculator.root.appendChild(calculator.history);
		calculator.root.appendChild(calculator.historyButtn);
		document.body.appendChild(calculator.root);
	}
	generateNumpadMarkup() {
		const numpad = this.createElem("div", {id: "numpad"});
		const buttonsText = [
		"C", "del", "", "/",
		"7", "8", "9", "*",
		"4", "5", "6", "-",
		"1", "2", "3", "+",
		"00", "0", ".", "="];

		buttonsText.forEach(elem => {
			let className;
			const operators = ["/","*","-","+"];

			if (elem === "C") {
				className = "clear-all";
			} else if (elem === "del") {
				className = "delete";
			} else if (elem === "") {
				className = "hidden-button";
			} else if (elem === "=") {
				className = "calculate";
			} else if (operators.includes(elem)) {
				className = "operator";
			}
			
			numpad.appendChild(
					this.createNumpadButtn(elem, className)	
			)
		});

		return numpad;
	}
	createElem(tagName, {text = "", ...attributes} = {}) {
		const elem = document.createElement(tagName)
		const textNode = document.createTextNode(text);
		Object.assign(elem, attributes);
		elem.appendChild(textNode);

		return elem;
	}
	createEquationDiv(text = "") {
		return this.createElem("div", {
			text,
			className: "equation"
		});
	}
	createNumpadButtn(text = "", additionClass = "") {
		return this.createElem("a", {
			text,
			className: `numpad-button ${additionClass}`,
			href: "#"
		});
	}
}