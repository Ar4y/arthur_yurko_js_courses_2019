class Calculator {
	constructor() {
		this._HTMLGenerator = new MarkupGenerator(this);
		this._isCalculated = false;

		Object.assign(this.display, displayMixin);
		Object.assign(this.history, historyMixin);

		this.historyButtn.addEventListener("click", this.history.slide.bind(this.history));
		this.numpad.addEventListener("click", this._numpadListener.bind(this));
	}
	round(number, precision) {
		const factor = Math.pow(10, precision);
		const tempNumber = number * factor;
		const roundedTempNumber = Math.round(tempNumber);
		return roundedTempNumber / factor;
	}
	_numpadListener(event) {
		const classList = event.target.classList;

		if (!classList.contains("numpad-button")) {
			return;
		}

		switch(true) {
			case classList.contains("clear-all"):
				if (this.display.getLastChild().innerText === "") {
					this.display.clear();
					this.history.clear();
				}
				this.display.getLastChild().innerText = "";
				this._isCalculated = false;
				break;

			case classList.contains("delete"):
				this.display.deleteLastSymbol();
				this._isCalculated = false;
				break;

			case classList.contains("calculate"):
				const result = eval(this.display.getLastChild().innerText);
				const roundedResult = this.round(result, 9);
				const nodeForHistory = this.display.getLastChild().cloneNode(true);
				nodeForHistory.innerText += `=${roundedResult}`;

				if (this.display.getLastChild().innerText === `${roundedResult}`) {
					return;
				}

				this.history.addElement(nodeForHistory);
				this.display.addElement(this._HTMLGenerator.createEquationDiv(roundedResult));
				this.history.updateHiddenPosition();
				this.display.scrollToBottom();
				this.history.scrollToBottom();
				this._isCalculated = true;
				break;

			case classList.contains("operator"):
				const innerText = this.display.getLastChild().innerText;
				const operators = ["/", "*", "-", "+"];
				const isLastSymbolOperator = operators.includes(innerText.substring(innerText.length - 1));

				if (event.target.innerText !== "-" && innerText === ""
					|| isLastSymbolOperator && innerText.length === 1) {
					return;
				}

				if (isLastSymbolOperator && innerText.length !== 1) {
					this.display.deleteLastSymbol();
				}

				this.display.getLastChild().innerText += event.target.innerText;
				this._isCalculated = false;
				break;

			default:
				if (this._isCalculated) {
					this.display.addElement(this._HTMLGenerator.createEquationDiv());
					this._isCalculated = false;
				}
				this.display.getLastChild().innerText += event.target.innerText;
				this.display.scrollToBottom();
				this.display.scrollToRight();
				break;
		}
	}
}

const calc = new Calculator();