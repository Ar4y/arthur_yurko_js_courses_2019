const displayMixin = {
	addElement(node) {
		return this.querySelector("div").appendChild(node);
	},
	getLastChild() {
		return this.querySelector("div").lastChild;
	},
	clear() {
		const displayChilds = [...this.querySelector("div").childNodes];
		this.addElement(
			displayChilds.reduce(
				(lastNode, node) => {
					lastNode = node.cloneNode();
					node.remove()
					return lastNode;
				}
			)
		);
	},
	deleteLastSymbol() {
		const lastChild = this.getLastChild();
		lastChild.innerText = lastChild.innerText.slice(0, -1);
	},
	scrollToBottom() {
		this.scrollTop = this.scrollHeight;
	},
	scrollToRight() {
		this.scrollLeft = this.scrollWidth;	
	}
}