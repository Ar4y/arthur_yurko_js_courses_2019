const historyMixin = {
	_isHidden: true,
	addElement(node) {
		return this.querySelector("div").appendChild(node);
	},
	clear() {
		const historyChilds = [...this.querySelector("div").childNodes];
		historyChilds.forEach(elem => elem.remove());
	},
	updateHiddenPosition() {
		if (!this._isHidden) {
			return;
		}
		const historyStyle = this.style;
		const timeout = 100;
		historyStyle.transition = "none";
		historyStyle.marginLeft = -this.offsetWidth + "px";
		setTimeout(() => {
			historyStyle.transition = "";
		}, timeout);
	},
	slide() {
		const style = this.style;
		if (this._isHidden) {
			style.marginLeft = "0px";
			style.opacity = 1;
			this._isHidden = false;
		} else {
			style.marginLeft = -this.offsetWidth + "px";
			style.opacity = 0;
			this._isHidden = true;
		}
	},
	scrollToBottom() {
		this.scrollTop = this.scrollHeight;
	}
}