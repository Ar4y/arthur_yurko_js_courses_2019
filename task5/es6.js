class Person {
	constructor(firstname, lastname, age) {
		this.firstname = this.checkName(firstname);
		this.lastname = this.checkName(lastname);
		this.age = this.checkAge(age);
	}

	checkName(name) {
		const regExpName = /^[a-zA-Z]{3,20}$/;
		if (regExpName.test(name)) {
			return name;
		}
		throw new Error("Invalid firstname or lastname");
	}

	checkAge(age) {
		if (age >= 0 && age <= 150) {
			return age;
		}
		throw new Error("Invalid age");
	}

	getFullname() {
		return `${this.firstname} ${this.lastname}`;
	}

	setFullname(fullname) {
		const firstLastNameArr = fullname.split(" ");
		firstLastNameArr[0] = this.checkName(firstLastNameArr[0]);
		firstLastNameArr[1] = this.checkName(firstLastNameArr[1]);
		this.firstname = firstLastNameArr[0];
		this.lastname = firstLastNameArr[1];
	}

	introduce() {
		return `Hello! My name is ${this.getFullname()} and I am ${this.age}-years-old`;
	}
}

class Worker extends Person {
	constructor(firstname, lastname, age, experience, salary) {
		super(firstname, lastname, age);
		this.experience = this.checkExperience(experience);
		this.salary = this.checkSalary(salary);
	}

	checkExperience(experience) {
		if (experience >= 1 && experience <= 50) {
			return experience;
		}
		throw new Error("Invalid experience");
	}

	checkSalary(salary) {
		if (salary >= 1000 && salary <= 10000) {
			return salary;
		}
		throw new Error("Invalid salary");
	}

	getRemunerationRate() {
		let procent;

		switch (true) {
			case this.experience <= 5:
				procent = 5;
				break;
			case this.experience <= 10:
				procent = 10;
				break;
		 	case this.experience <= 20:
				procent = 20;
				break;
		 	default: 
				procent = 50;
		}

		return this.experience * procent / 100;
	}
}