// Масcив юзеров 
const users = [
    {
        balance: '3946.45',
        picture: 'http://placehold.it/32x32',
        age: 23,
        name: 'Bird Ramsey',
        gender: 'male',
        company: 'NIMON',
        email: 'birdramsey@nimon.com',
    },
    {
        balance: '2499.49',
        picture: 'http://placehold.it/32x32',
        age: 31,
        gender: 'female',
        company: 'LUXURIA',
        email: 'lillianburgess@luxuria.com',
    },
    {
        balance: '2820.18',
        picture: 'http://placehold.it/32x32',
        age: 34,
        name: 'Kristie Cole',
        gender: 'female',
        company: 'QUADEEBO',
        email: 'kristiecole@quadeebo.com',
    },
    {
        balance: '1277.32',
        picture: 'http://placehold.it/32x32',
        age: 30,
        gender: 'female',
        company: 'GRONK',
        email: 'leonorcross@gronk.com',
    },
    {
        balance: '1972.47',
        picture: 'http://placehold.it/32x32',
        age: 28,
        gender: 'male',
        company: 'ULTRIMAX',
        email: 'marshmccall@ultrimax.com',
    },
];

/*
Task 1
Написать функцию getNameFromCurrentContex, которя будет брать значение поля name з своего контекста вызова.
Далее реализуем функцию rejectNoNameUsers, в которую передаем массив users - проходимся по каждому елементу масива users,
и в контексте каждного юзера вызываем функцию getNameFromCurrentContex.  Если функция getNameFromCurrentContex вернула
undefined, тогда даляем юзера из массива. Результатом работы rejectNoNameUsers будет НОВЫЙ массив юзеров, у которых есть поле name. 
*/

function rejectNoNameUsers(usersArray) {
    return usersArray.filter((user) => {
        return getNameFromCurrentContex.call(user);
    });
}

function getNameFromCurrentContex() {
    return this.name;
}

console.log(rejectNoNameUsers(users));
